package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.Category;
import com.example.demo.service.category.CategoryService;

@Controller
public class CategoryController {

	private CategoryService categoryService;

	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping("/category")
	public String category(Model m) {
		m.addAttribute("category", new Category());
		m.addAttribute("allCate", categoryService.findAll());
		m.addAttribute("formAdd", true);

		return "category";
	}

	@PostMapping("/addCategory")
	public String addCategory(@Valid @ModelAttribute Category category, BindingResult result, Model m) {

		System.out.println(result.hasErrors());

		if (result.hasErrors()) {
			m.addAttribute("category", category);
			m.addAttribute("allCate", categoryService.findAll());
			m.addAttribute("formAdd", true);
			return "redirect:/category";
		}
		categoryService.addCategory(category);
		return "redirect:/category";
	}

	@GetMapping("/updateCategory/{id}")
	public String updateCategory(Model m, @PathVariable int id) {
		// System.out.println(id);
		m.addAttribute("category", categoryService.findOne(id));
		m.addAttribute("allCate", categoryService.findAll());
		m.addAttribute("formAdd", false);
		return "category";
	}

	@PostMapping("/updateCategory")
	public String updateCategory(@Valid @ModelAttribute Category category, BindingResult result, Model m) {

		if (result.hasErrors()) {
			m.addAttribute("category", category);
			m.addAttribute("allCate", categoryService.findAll());
			m.addAttribute("formAdd", true);
			return "redirect:/category";
		}

		categoryService.updateCatory(category);
		return "redirect:/category";
	}

	@GetMapping("/deleteCategory/{id}")
	public String delete(@PathVariable("id") int id) {
		categoryService.deleteCategory(id);
		return "redirect:/category";
	}
}

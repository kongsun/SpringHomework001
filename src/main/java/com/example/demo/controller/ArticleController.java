package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.Article;
import com.example.demo.service.article.*;
import com.example.demo.service.category.CategoryService;

@Controller
public class ArticleController {

	private ArticleService articleService;
	private CategoryService categoryService;

	@Autowired
	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}

	@Autowired
	public void setCategoryservice(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping({ "/article", "/", "" })
	public String article(ModelMap m) {
		List<Article> articles = articleService.findAll();
		System.out.println(articles);
		m.addAttribute("categories", categoryService.findAll());
		m.addAttribute("articles", articles);
		return "article";
	}

	@GetMapping("/add")
	public String add(ModelMap m) {
		m.addAttribute("article", new Article());
		m.addAttribute("categories", categoryService.findAll());
		m.addAttribute("formAdd", true);
		return "add";
	}

	@PostMapping("/add")
	public String save(@Valid @ModelAttribute Article article, BindingResult result, ModelMap m,
			@RequestParam("file") MultipartFile file) {
		article.setDate(new Date().toString());
		if (result.hasErrors()) {
			m.addAttribute("article", article);
			m.addAttribute("categories", categoryService.findAll());
			m.addAttribute("formAdd", true);
			return "add";
		}
		String fileName = "";
		String pathFile = "/Users/kongsun123/Desktop/image/";
		if (!file.isEmpty()) {
			try {
				fileName = UUID.randomUUID() + "."
						+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
				Files.copy(file.getInputStream(), Paths.get(pathFile, fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		article.setThumnailURL("/image/" + fileName);
		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		articleService.add(article);
		return "redirect:/add";
	}

	@GetMapping("/update/{id}")
	public String update(@PathVariable int id, ModelMap m) {
		m.addAttribute("article", articleService.findOne(id));
		m.addAttribute("categories", categoryService.findAll());
		m.addAttribute("formAdd", false);
		return "add";
	}

	@PostMapping("/update")
	public String saveUpdate(@Valid @ModelAttribute Article article, BindingResult result, ModelMap m,
			@RequestParam("file") MultipartFile file) {

		article.setDate(new Date().toString());
		if (result.hasErrors()) {
			m.addAttribute("article", article);
			m.addAttribute("categories", categoryService.findAll());
			m.addAttribute("formAdd", false);
			return "add";
		}
		String fileName = "";
		String pathFile = "/Users/kongsun123/Desktop/image/";

		if (!file.isEmpty()) {
			try {
				fileName = UUID.randomUUID() + "."
						+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
				Files.copy(file.getInputStream(), Paths.get(pathFile, fileName));
				System.out.println(fileName);
			} catch (IOException e) {
				e.printStackTrace();
			}
			article.setThumnailURL("/image/" + fileName);
		} else {
			article.setThumnailURL(article.getThumnailURL());
		}

		article.setCategory(categoryService.findOne(article.getCategory().getId()));
		articleService.update(article);
		System.out.println(article);
		return "redirect:/article";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}

	@GetMapping("/view/{id}")
	public String view(ModelMap m, @PathVariable("id") int id) {
		m.addAttribute("article", articleService.findOne(id));
		return "view";
	}
}

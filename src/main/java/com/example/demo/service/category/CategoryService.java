package com.example.demo.service.category;

import java.util.List;

import com.example.demo.model.Category;

public interface CategoryService {
	void addCategory(Category category);

	void updateCatory(Category category);

	List<Category> findAll();

	Category findOne(int id);

	void deleteCategory(int id);
}

package com.example.demo.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Category;
import com.example.demo.repository.category.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	private CategoryRepository categoryRepo;
	@Autowired
	private void setCategoryService(CategoryRepository categoryRepo) {
		this.categoryRepo=categoryRepo;
	}
	
	@Override
	public List<Category> findAll() {	
		return categoryRepo.findAll();
	}

	@Override
	public Category findOne(int id) {
		// TODO Auto-generated method stub
		return categoryRepo.findOne(id);
	}

	@Override
	public void addCategory(Category category) {
		categoryRepo.addCategory(category);
	}

	@Override
	public void updateCatory(Category category) {
		categoryRepo.updateCategory(category);
		
	}

	@Override
	public void deleteCategory(int id) {
		categoryRepo.deleteCategory(id);
		
	}

}

package com.example.demo.service.article;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.Article;
import com.example.demo.repository.article.ArticleRepository;

@Service
public class ArticleServiceImpl implements ArticleService {

	private ArticleRepository articleRepo;

	@Autowired
	public void setArticleRepo(ArticleRepository articleRepo) {
		this.articleRepo = articleRepo;
	}

	@Override
	public Article findOne(int id) {
		// TODO Auto-generated method stub
		return articleRepo.findOne(id);
	}

	@Override
	public void add(Article article) {
		// TODO Auto-generated method stub
		articleRepo.add(article);
	}

	@Override
	public List<Article> findAll() {
		// TODO Auto-generated method stub
		return articleRepo.findAll();
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		articleRepo.delete(id);
	}

	@Override
	public void update(Article article) {
		// TODO Auto-generated method stub
		articleRepo.update(article);
	}
}

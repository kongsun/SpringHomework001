package com.example.demo.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Category;

@Repository
public interface CategoryRepository {

	@Insert("INSERT INTO tb_category(name) VALUES (#{name})")
	void addCategory(Category category);

	@Update("UPDATE tb_category SET name=#{name} WHERE id=#{id}")
	void updateCategory(Category category);

	@Select("SELECT id,name FROM tb_category ORDER BY id ASC")
	List<Category> findAll();

	@Select("SELECT id,name FROM tb_category WHERE id=#{id}")
	Category findOne(int id);

	@Delete("DELETE FROM tb_category WHERE id=#{id}")
	void deleteCategory(int id);
}

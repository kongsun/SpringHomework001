package com.example.demo.repository.category;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.model.Category;

//@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
	private List<Category> categories = new ArrayList<Category>();

	public CategoryRepositoryImpl() {
		categories.add(new Category(1, "Java"));
		categories.add(new Category(2, "Web"));
		categories.add(new Category(3, "Spring"));
		categories.add(new Category(4, "Korea"));
		categories.add(new Category(5, "Database"));
	}

	@Override
	public List<Category> findAll() {
		return categories;
	}

	@Override
	public Category findOne(int id) {
		for (Category c : categories) {
			if (c.getId() == id) {
				return c;
			}
		}
		return null;
	}

	@Override
	public void addCategory(Category category) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCategory(Category category) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCategory(int id) {
		// TODO Auto-generated method stub

	}

}

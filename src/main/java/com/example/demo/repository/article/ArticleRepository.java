package com.example.demo.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;

@Repository

public interface ArticleRepository {

	@Insert("INSERT INTO tb_articles(title,author,description,created_date,thumnailurl,category_id)"
			+ "VALUES(#{name},#{author},#{description},#{date},#{thumnailURL},#{category.id})")
	void add(Article article);

	@Select("SELECT a.id,a.title,a.author,a.description,a.created_date,a.thumnailurl, a.category_id, c.name "
			+ "FROM tb_articles a INNER JOIN tb_category c ON a.category_id = c.id WHERE a.id=#{id} ORDER BY a.id ASC")

	@Results({ @Result(property = "id", column = "id"), @Result(property = "name", column = "title"),
			@Result(property = "author", column = "author"), @Result(property = "description", column = "description"),
			@Result(property = "date", column = "created_date"),
			@Result(property = "thumnailURL", column = "thumnailurl"),
			@Result(property = "category.id", column = "category_id"),
			@Result(property = "category.name", column = "name") })
	Article findOne(int id);

	@Select("SELECT a.id,a.title,a.author,a.description,a.created_date,a.thumnailurl, a.category_id, c.name "
			+ "FROM tb_articles a INNER JOIN tb_category c ON a.category_id = c.id ORDER BY a.id ASC")
	@Results({ @Result(property = "id", column = "id"), @Result(property = "name", column = "title"),
			@Result(property = "author", column = "author"), @Result(property = "description", column = "description"),
			@Result(property = "date", column = "created_date"),
			@Result(property = "thumnailURL", column = "thumnailurl"),
			@Result(property = "category.id", column = "category_id"),
			@Result(property = "category.name", column = "name") })
	List<Article> findAll();

	@Delete("DELETE FROM tb_articles WHERE id=#{id}")
	void delete(int id);

	@Update("UPDATE tb_articles SET title=#{name}," + "author=#{author}," + "description=#{description},"
			+ "category_id=#{category.id}, " + "thumnailURL=#{thumnailURL}" + "WHERE id=#{id}")
	void update(Article article);
}

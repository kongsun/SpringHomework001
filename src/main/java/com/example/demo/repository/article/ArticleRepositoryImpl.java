package com.example.demo.repository.article;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.demo.model.Article;
import com.example.demo.model.Category;
import com.github.javafaker.Faker;

//@Repository
public class ArticleRepositoryImpl implements ArticleRepository {

	private List<Article> articles = new ArrayList<>();

	public ArticleRepositoryImpl() {
		Faker faker = new Faker();
		for (int i = 0; i < 5; i++) {
			articles.add(new Article(i, faker.book().title(), faker.book().author(), faker.book().publisher(),
					new Date().toString(), faker.internet().image(), new Category(6, "Thymeleaf")));
		}
	}

	@Override
	public void add(Article article) {
		articles.add(article);
	}

	@Override
	public Article findOne(int id) {
		for (Article article : articles) {
			if (article.getId() == id) {
				return article;
			}
		}
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		for (Article article : articles) {
			if (article.getId() == id) {
				articles.remove(article);
				return;
			}
		}
	}

	@Override
	public List<Article> findAll() {
		// TODO Auto-generated method stub
		return articles;
	}

	@Override
	public void update(Article article) {
		// TODO Auto-generated method stub
		for (int i = 0; i < articles.size(); i++) {
			if (articles.get(i).getId() == article.getId()) {
				articles.get(i).setName(article.getName());
				articles.get(i).setAuthor(article.getAuthor());
				articles.get(i).setDescription(article.getDescription());

				if (article.getThumnailURL() != null) {
					articles.get(i).setThumnailURL(article.getThumnailURL());
				} else
					return;
				articles.get(i).setCategory(article.getCategory());
				return;
			}
		}
	}

}

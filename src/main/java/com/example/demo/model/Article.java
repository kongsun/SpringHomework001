package com.example.demo.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Article {

	@NotNull
	private int id;
	@NotBlank
	private String name;
	@NotBlank
	private String author;
	@NotBlank
	private String description;
	private String date;
	private String thumnailURL;
	private Category category;

	public Article() {
	}

	public Article(@NotNull int id, @NotBlank String name, @NotBlank String author, @NotBlank String description,
			String date, String thumnailURL, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
		this.description = description;
		this.date = date;
		this.thumnailURL = thumnailURL;
		this.category = category;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getThumnailURL() {
		return thumnailURL;
	}

	public void setThumnailURL(String thumnailURL) {
		this.thumnailURL = thumnailURL;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", name=" + name + ", author=" + author + ", description=" + description
				+ ", date=" + date + ", thumnailURL=" + thumnailURL + ", category=" + category + "]";
	}

}

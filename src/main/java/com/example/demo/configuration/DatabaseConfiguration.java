package com.example.demo.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DatabaseConfiguration {

	@Bean
	@Profile("database")
	public DataSource dataSource() {
		DriverManagerDataSource dvgDataSource = new DriverManagerDataSource();
		dvgDataSource.setDriverClassName("org.postgresql.Driver");
		dvgDataSource.setUrl("jdbc:postgresql://localhost:5432/ams");
		dvgDataSource.setUsername("postgres");
		dvgDataSource.setPassword("Kongsun123");
		return dvgDataSource;
	}

	@Bean
	@Profile("memory")
	public DataSource development() {
		EmbeddedDatabaseBuilder edb = new EmbeddedDatabaseBuilder();
		edb.setType(EmbeddedDatabaseType.H2);
		edb.addScript("sql/table.sql");
		edb.addScript("sql/data.sql");
		return edb.build();
	}

}

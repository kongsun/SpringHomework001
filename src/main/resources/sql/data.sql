INSERT INTO tb_category(name) VALUES('Spring');
INSERT INTO tb_category(name) VALUES('Korea');
INSERT INTO tb_category(name) VALUES('Web');
INSERT INTO tb_category(name) VALUES('Java');
INSERT INTO tb_category(name) VALUES('Thymeleaf');

INSERT INTO TB_ARTICLES (title, author, description, created_date, thumnailurl, category_id) VALUES('Spring is powerful', 'Jonh', 'Powerful Java Framework', 'Sun Jun 24 10:50:44 ICT 2018', 'https://www.packtpub.com/sites/default/files/B05941_cover.png', 1);
INSERT INTO TB_ARTICLES (title, author, description, created_date, thumnailurl, category_id) VALUES('Java for beginner', 'Rinda', 'Kid also learn this', 'Sun Jun 24 10:50:44 ICT 2018', 'https://www.packtpub.com/sites/default/files/9781786468734.png', 4);
INSERT INTO TB_ARTICLES (title, author, description, created_date, thumnailurl, category_id) VALUES('Songang Korean Ep1', 'Jimmy', 'Hankokor Khong Pu Haeyo', 'Sun Jun 24 10:50:44 ICT 2018', 'http://cdn2.bigcommerce.com/server5500/czd793/products/306/images/1770/book_kor_1274939048_90310__93830.1366599894.450.550.jpg?c=2', 2);
INSERT INTO TB_ARTICLES (title, author, description, created_date, thumnailurl, category_id) VALUES('Thymeleaf', 'Donald', 'A leaf without physic', 'Sun Jun 24 10:50:44 ICT 2018', 'https://images-eu.ssl-images-amazon.com/images/I/51MXg7pQ5IL.jpg', 5);
INSERT INTO TB_ARTICLES (title, author, description, created_date, thumnailurl, category_id) VALUES('Web Front-end', 'Benica', 'Expert you design skill', 'Sun Jun 24 10:50:44 ICT 2018', 'https://i.pinimg.com/originals/02/99/3e/02993e7f570d443fbda43090a8909367.png', 3);
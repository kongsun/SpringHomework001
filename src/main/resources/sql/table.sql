create table tb_category(
  id int PRIMARY key auto_increment,
  name varchar(40) not null
);

create table tb_articles(
  id int primary key auto_increment,
  title varchar(40) not null,
  author varchar(40) not null ,
  description text not null ,
  created_date varchar(30) not null,
  thumnailURL varchar not null,
  category_id int REFERENCES tb_category (id) on DELETE CASCADE ON UPDATE CASCADE
)
